# Chatbot
 A Chatbot that searches for the restaurants according to users preferred location cuisine and budget. On user's wish it also mails the top 10 restaurants.
 
# Versions of the modules
rasa-core=0.10.1
python=3.6.4
rasa-nlu=0.12.2